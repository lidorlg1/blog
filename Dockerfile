FROM golang:1.14-alpine AS base

RUN mkdir /app

RUN apk update
RUN apk upgrade
RUN apk add --no-cache bash git

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.69.1/hugo_0.69.1_Linux-32bit.tar.gz -O /usr/local/bin/hugo.tar.gz
RUN wget https://github.com/caddyserver/caddy/releases/download/v2.0.0-rc.3/caddy_2.0.0-rc.3_linux_amd64.tar.gz -O /usr/local/bin/caddy.tar.gz

WORKDIR /usr/local/bin
RUN tar -xvf hugo.tar.gz
RUN tar -xvf caddy.tar.gz

FROM golang:1.14-alpine AS dev
COPY --from=base /usr/local/bin/hugo /usr/local/bin/hugo
RUN hugo version

FROM golang:1.14-alpine AS final
COPY --from=base /usr/local/bin/hugo /usr/local/bin/hugo
COPY --from=base /usr/local/bin/caddy /usr/local/bin/caddy

COPY . /app/
RUN hugo version
RUN cd /app && hugo

HEALTHCHECK --interval=5s --timeout=5s --retries=10 \
  CMD wget --quiet --tries=1 --spider http://localhost || exit 1

WORKDIR /app/public

EXPOSE 80

CMD ["file-server"]

ENTRYPOINT [ "caddy" ]
