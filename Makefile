SHELL := /bin/sh
.DEFAULT_GOAL:=help
.PHONY: help dev test

build-dev: ## Build the development image
	@docker-compose build

dev: ## Run the development stack, it automatically reloads on change
	@docker-compose up

help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
